# Манифест разработчиков платформы СоцКооп

## Введение

Цель платформы СоцКооп — построение высокотехнологичной и общедоступной площадки для развития потребительской кооперации в России и других странах.

При разработке платформы мы придерживаемся принципов открытого программного обеспечения и делаем доступными для всех заинтересованных пользователей не только исходный код, но и все этапы разработки, начиная с продуктового понимания и заканчивая внедрением инфраструктуры.

Открытость — это наша основная ценность.

С одной стороны, она даёт нам расширяемость и развитие проекта, мы не хотим замыкаться на собственном представлении и препятствовать творческой трансформации и актуализации наших идей.

А с другой — это аккумуляция полезных знаний: наша команда стремится к созданию широкого сообщества разработчиков и к активному обмену опытом.

## Используемые подходы к разработке

При попытке создания такой платформы одной из главных задач является решение проблемы слабой коммуникации и вовлечения участников. Проект СоцКооп исключительно некоммерческий, его ресурсы непостоянны, а участники имеют разный запас свободного времени, владеют разными компетенциями и находятся в разных часовых поясах.

Именно поэтому для организации коллективной асинхронной работы мы решили использовать определённые подходы к проектированию и разработке, которые позволят лучше задействовать имеющиеся ресурсы и помогут людям на проекте найти общий язык.

Не стоит рассматривать используемые подходы как серебряную пулю, способную решить все проблемы. Тем более, что далеко не всегда они используются буквально, как есть. 

### Domain Driven Design

Предметно-ориентированное проектирование — подход, который предполагает отталкивание от интересов предметной области. В случае коммерческой разработки это интересы бизнеса, а в нашем случае — интересы потребительской кооперации.

При проектировании системы в рамках DDD выделяются две взаимосвязанные ключевые задачи, решение которых предоставляет мощнейший инструментарий для работы с проектом:

Во-первых, это выделение контекстов (bounded context). Один контекст — это ограниченная часть системы, в рамках которой реализуется определённая бизнес-логика. Выделение контекстов позволяет органически разделить проект на модули, упрощает их разработку и развитие и минимизирует запутанность.

Во-вторых, это формирование единого языка (ubiquitous language). Это набор терминов, понятий и даже определённых фраз, выполняющих коммуникативную функцию между разработчиками, экспертами и, в нашем случае, всеми заинтересованными участниками. Он позволяет максимально отражать бизнес-цели и минимизирует ошибки в коммуникации.

Обе эти задачи тесно взаимосвязаны: контекст предметной области определяется языком, а сформированный язык приобретает смысл благодаря определённому контексту. При этом не стоит недооценивать задачу формирования единого языка, потому что эффективная коммуникация — это краеугольный камень коллективной деятельности. 

Для более подробного изучения мы рекомендуем следующие материалы:

- [Что можно узнать о Domain Driven Design за 10 минут?](https://habr.com/ru/companies/dododev/articles/489352/) — краткая обзорная статья на Хабре;
- [Очень технический выпуск: про DDD и проектирование сложных систем](https://habr.com/ru/companies/redmadrobot/articles/542142/) — статья на Хабре, содержит очень много полезного материала для изучения в конце;
- «Domain-Driven Design Distilled» Vernon, Vaughn — книга для начинающих

### Specification By Example

Specification By Example — это более низкоуровневый инструмент, дополняющий DDD. Он позволяет разбивать контексты на функциональные требования (features) и декомпозировать их на сценарии (scenarios).

Каждый сценарий описывается в отдельном файле, обычно с использованием Gherkin — человеко-читаемого языка для описания поведения системы, так как он достаточно прост и удобен. Тут-то и пригождается DDD: единый язык и рамки контекста позволяют использовать ёмкие определения и сохранять содержимое достаточно независимым.  

Таким образом формируется набор спецификаций, совокупность которых не только исчерпывающе описывает систему, но и является готовым техническим заданием для всех разработчиков, от UX-дизайнеров и фронтенд-разработчиков до аналитиков и тестировщиков.

Для более подробного изучения мы рекомендуем следующие материалы:

- [Spec By Example на примере одного требования](https://habr.com/ru/companies/scrumtrek/articles/170919/) — небольшая обзорная статья на Хабре;
- «Writing Great Specifications: Using Specification by Example and Gherkin» Nicieja, Kamil — литература по теме.

## Текущее положение дел

На текущий момент проект находится на стадии инициализации: мы активно собираем сообщество и ресурсы, проводим базовую автоматизацию. Разработка будет вестись в сервисе GitLab с помощью коллективно обсуждаемых задач (issues).

Пока что основной язык проекта — русский: спецификации, описания задач и прочие артефакты будут написаны на русском языке. Конечно, кроме исходного кода.

Актуальные планы:

- Разработка SBE-спецификаций. По сути, проектирование платформы, определение контекстов и выработка единого языка.
- Автоматизация разработки. Выбор, разворачивание и налаживание инфраструктуры платформы.

Вы можете вступить в [телеграм-чат платформы](https://t.me/+KQSIAmmCGNg1MmUy) и присоединиться к работе.

[Проект в GitLab](https://gitlab.com/soccoop-platform)

## Ближайшие планы на развитие

[**Раздел в разработке**]

В этом разделе будут описаны ближайшие планы на развитие, которые ещё не отражены в конкретных задачах и спецификациях, но уже готовы появиться как отдельные фичи или проекты в рамках платформы.

## Введение в проект: основные принципы

[**Раздел в разработке**]

В этом разделе будут более детально описаны принципы работы в сервисе GitLab, а также даны ссылки на небольшие видео-инструкции для тех, кто впервые столкнулся с git-репозиториями и GitLab в частности.
